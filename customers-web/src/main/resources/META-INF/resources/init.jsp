<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.arcusys.test.customers.model.Customer" %><%@
page import="com.arcusys.test.customers.service.CustomerLocalService" %><%@
page import="com.arcusys.test.customers.model.Address" %><%@
page import="com.arcusys.test.customers.service.AddressLocalService" %><%@
page import="com.arcusys.test.customers.model.Phone" %><%@
page import="com.arcusys.test.customers.service.PhoneLocalService" %><%@
page import="com.arcusys.test.customers.model.Email" %><%@
page import="com.arcusys.test.customers.service.EmailLocalService" %><%@
page import="com.arcusys.test.customers.service.EntityTypes" %><%@
page import="com.liferay.portal.kernel.util.Constants" %><%@
page import="com.liferay.portal.kernel.util.ParamUtil" %><%@
page import="com.liferay.portal.kernel.util.StringPool" %><%@
page import="com.liferay.portal.kernel.util.WebKeys" %><%@
page import="com.liferay.taglib.search.ResultRow" %>

<%@ page import="javax.portlet.PortletURL" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
PortletURL portletURL = renderResponse.createRenderURL();

String currentURL = portletURL.toString();

//get service beans
CustomerLocalService customerLocalService = (CustomerLocalService)request.getAttribute("customerLocalService");
AddressLocalService addressLocalService = (AddressLocalService)request.getAttribute("addressLocalService");
PhoneLocalService phoneLocalService = (PhoneLocalService)request.getAttribute("phoneLocalService");
EmailLocalService emailLocalService = (EmailLocalService)request.getAttribute("emailLocalService");
%>
