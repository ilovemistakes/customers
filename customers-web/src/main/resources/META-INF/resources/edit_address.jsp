<%@ include file="/init.jsp" %>

<%
String redirect = ParamUtil.getString(request, "redirect");

long addressId = ParamUtil.getLong(request, "addressId");
long customerId = ParamUtil.getLong(request, "customerId");

Address address = null;

if (addressId > 0) {
	address = addressLocalService.getAddress(addressId);
}
%>

<aui:form action="<%= renderResponse.createActionURL() %>" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= address == null ? Constants.ADD : Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= currentURL %>" />
	<aui:input name="addressId" type="hidden" value="<%= addressId %>" />
	<aui:input name="customerId" type="hidden" value="<%= customerId %>" />

	<liferay-ui:header
		backURL="<%= redirect %>"
		title='<%= (address != null) ? "edit-address" : "new-address" %>'
	/>

	<aui:model-context bean="<%= address %>" model="<%= Address.class %>" />

        <aui:fieldset-group markupView="lexicon">
                <aui:fieldset>
                        <aui:field-wrapper name="Type" required="true">
                                <aui:input inlineField="true" name="type" label="Visiting" type="radio" value="<%= EntityTypes.VISITING_ADDRESS %>" checked="<%= (address == null) ? true : (address.getType() == EntityTypes.VISITING_ADDRESS) %>">
                                        <aui:validator name="required" />
                                </aui:input>
                                <aui:input inlineField="true"  name="type" label="Postal" type="radio" value="<%= EntityTypes.POSTAL_ADDRESS %>" checked="<%= (address == null) ? false : (address.getType() == EntityTypes.POSTAL_ADDRESS) %>"/>
                        </aui:field-wrapper>
                        <aui:input name="town" label="Town">
                                <aui:validator name="required" />
                        </aui:input>
                        <aui:input name="street" label="Street line 1">
                                <aui:validator name="required" />
                        </aui:input>
                        <aui:input name="street2" label="Street line 2"/>
                        <aui:input name="postalCode">
                                <aui:validator name="required" />
                        </aui:input>
                </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
                <aui:button type="submit" />

                <aui:button href="<%= redirect %>" type="cancel" />
        </aui:button-row>
</aui:form>
