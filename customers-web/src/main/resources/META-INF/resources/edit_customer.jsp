<%@ include file="/init.jsp" %>

<%
String redirect = ParamUtil.getString(request, "redirect");

long customerId = ParamUtil.getLong(request, "customerId");

Customer customer = null;

if (customerId > 0) {
	customer = customerLocalService.getCustomer(customerId);
}
%>

<aui:form action="<%= renderResponse.createActionURL() %>" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= customer == null ? Constants.ADD : Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= currentURL %>" />
	<aui:input name="customerId" type="hidden" value="<%= customerId %>" />

	<liferay-ui:header
		backURL="<%= redirect %>"
		title='<%= (customer != null) ? customer.getName() : "new-customer" %>'
	/>

	<aui:model-context bean="<%= customer %>" model="<%= Customer.class %>" />

        <aui:fieldset-group markupView="lexicon">
                <aui:fieldset>
                        <aui:input name="name" autoFocus="true">
                                <aui:validator name="required" />
                        </aui:input>

                        <aui:input name="notes" />

                        <liferay-ui:custom-attributes-available className="<%= Customer.class.getName() %>">
                                <liferay-ui:custom-attribute-list
                                        className="<%= Customer.class.getName() %>"
                                        classPK="<%= (customer != null) ? customer.getCustomerId() : 0 %>"
                                        editable="<%= true %>"
                                        label="<%= true %>"
                                />
                        </liferay-ui:custom-attributes-available>
                </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
                <aui:button type="submit" />

                <aui:button href="<%= redirect %>" type="cancel" />
        </aui:button-row>
</aui:form>
