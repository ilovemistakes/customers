<%@ include file="/init.jsp" %>

<%
String redirect = ParamUtil.getString(request, "redirect");

long phoneId = ParamUtil.getLong(request, "phoneId");
long customerId = ParamUtil.getLong(request, "customerId");

Phone phone = null;

if (phoneId > 0) {
	phone = phoneLocalService.getPhone(phoneId);
}
%>

<aui:form action="<%= renderResponse.createActionURL() %>" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= phone == null ? Constants.ADD : Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= currentURL %>" />
	<aui:input name="phoneId" type="hidden" value="<%= phoneId %>" />
	<aui:input name="customerId" type="hidden" value="<%= customerId %>" />

	<liferay-ui:header
		backURL="<%= redirect %>"
		title='<%= (phone != null) ? "edit-phone" : "new-phone" %>'
	/>

	<aui:model-context bean="<%= phone %>" model="<%= Phone.class %>" />

        <aui:fieldset-group markupView="lexicon">
                <aui:fieldset>
                        <aui:field-wrapper name="Type" required="true">
                                <aui:input inlineField="true" name="type" label="Mobile" type="radio" value="<%= EntityTypes.MOBILE_PHONE %>" checked="<%= (phone == null) ? true : (phone.getType() == EntityTypes.MOBILE_PHONE) %>">
                                        <aui:validator name="required" />
                                </aui:input>
                                <aui:input inlineField="true"  name="type" label="Work" type="radio" value="<%= EntityTypes.WORK_PHONE %>" checked="<%= (phone == null) ? false : (phone.getType() == EntityTypes.WORK_PHONE) %>"/>
                                <aui:input inlineField="true"  name="type" label="Home" type="radio" value="<%= EntityTypes.HOME_PHONE %>" checked="<%= (phone == null) ? false : (phone.getType() == EntityTypes.HOME_PHONE) %>"/>
                        </aui:field-wrapper>
                        <aui:input name="value" label="Phone number">
                                <aui:validator name="required" />
                                <aui:validator name="custom" errorMessage="Please format the phone number as +1234567890">
                                function (val, fieldNode, ruleValue) {
                                        var phoneRegexStr = /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}$/;
                                        return phoneRegexStr.test(val);
                                }
                                </aui:validator>
                                <!-- use libphonenumber for validation and common display format -->
                        </aui:input>
                </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
                <aui:button type="submit" />

                <aui:button href="<%= redirect %>" type="cancel" />
        </aui:button-row>
</aui:form>
