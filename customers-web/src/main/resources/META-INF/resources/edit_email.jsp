<%@ include file="/init.jsp" %>

<%
String redirect = ParamUtil.getString(request, "redirect");

long emailId = ParamUtil.getLong(request, "emailId");
long customerId = ParamUtil.getLong(request, "customerId");

Email email = null;

if (emailId > 0) {
	email = emailLocalService.getEmail(emailId);
}
%>

<aui:form action="<%= renderResponse.createActionURL() %>" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= email == null ? Constants.ADD : Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= currentURL %>" />
	<aui:input name="emailId" type="hidden" value="<%= emailId %>" />
	<aui:input name="customerId" type="hidden" value="<%= customerId %>" />

	<liferay-ui:header
		backURL="<%= redirect %>"
		title='<%= (email != null) ? "edit-email" : "new-email" %>'
	/>

	<aui:model-context bean="<%= email %>" model="<%= Email.class %>" />

        <aui:fieldset-group markupView="lexicon">
                <aui:fieldset>
                        <aui:field-wrapper name="Type" required="true">
                                <aui:input inlineField="true" name="type" label="Personal" type="radio" value="<%= EntityTypes.PERSONAL_EMAIL %>" checked="<%= (email == null) ? true : (email.getType() == EntityTypes.PERSONAL_EMAIL) %>">
                                        <aui:validator name="required" />
                                </aui:input>
                                <aui:input inlineField="true"  name="type" label="Work" type="radio" value="<%= EntityTypes.WORK_EMAIL %>" checked="<%= (email == null) ? false : (email.getType() == EntityTypes.WORK_EMAIL) %>"/>
                        </aui:field-wrapper>
                        <aui:input name="value" label="E-mail address">
                                <aui:validator name="required" />
                                <aui:validator name="email" />
                        </aui:input>
                </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
                <aui:button type="submit" />

                <aui:button href="<%= redirect %>" type="cancel" />
        </aui:button-row>
</aui:form>
