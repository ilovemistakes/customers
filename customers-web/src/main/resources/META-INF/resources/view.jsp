<%@ include file="/init.jsp" %>

<p>
	<b><liferay-ui:message key="customersportlet_Customersportlet.caption"/></b>
</p>

<liferay-ui:success key="operation-success" message="operation-success" />

<aui:button-row>
	<portlet:renderURL var="editCustomerURL">
		<portlet:param name="mvcPath" value="/edit_customer.jsp" />
		<portlet:param name="redirect" value="<%= currentURL %>" />
	</portlet:renderURL>

	<aui:button href="<%= editCustomerURL %>" value="add-customer" />
</aui:button-row>

<liferay-ui:search-container
	total="<%= customerLocalService.getCustomersCount() %>"
>
	<liferay-ui:search-container-results
		results="<%= customerLocalService.getCustomers(searchContainer.getStart(), searchContainer.getEnd()) %>"
	/>

	<liferay-ui:search-container-row
		className="com.arcusys.test.customers.model.Customer"
		escapedModel="true"
		modelVar="customer"
	>
		<liferay-ui:search-container-column-text
			name="name"
			valign="top"
		>
			<strong><%= customer.getName() %></strong>
		</liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-jsp
			name="addresses"
			valign="top"
                        path="/customer_address_list.jsp"
		/>

		<liferay-ui:search-container-column-jsp
			name="phones"
			valign="top"
                        path="/customer_phone_list.jsp"
		/>

		<liferay-ui:search-container-column-jsp
			name="emails"
			valign="top"
                        path="/customer_email_list.jsp"
		/>

		<liferay-ui:search-container-column-text
			name="notes"
			property="notes"
			valign="top"
		/>

		<liferay-ui:search-container-column-jsp
			cssClass="entry-action"
			path="/customer_action.jsp"
			valign="top"
		/>
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator />
</liferay-ui:search-container>
