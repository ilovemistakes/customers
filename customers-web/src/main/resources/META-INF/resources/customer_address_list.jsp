<%@ include file="/init.jsp" %>

<%
ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

Customer customer = null;

if (row != null) {
	customer = (Customer)row.getObject();
}
else {
	customer = (Customer)request.getAttribute("edit_customer.jsp-customer");
}
%>

<c:set value="<%= addressLocalService.findByCustomerId(customer.getCustomerId()) %>" var="addresses" />
<c:set var="POSTAL_ADDRESS" value="<%= EntityTypes.POSTAL_ADDRESS %>" />
<c:set var="VISITING_ADDRESS" value="<%= EntityTypes.VISITING_ADDRESS %>" />

<c:forEach items="${addresses}" var="address">
	<portlet:renderURL var="redirectURL">
		<portlet:param name="mvcPath" value="/view.jsp" />
	</portlet:renderURL>

	<portlet:actionURL var="deleteURL">
		<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.DELETE %>" />
		<portlet:param name="redirect" value="<%= (row == null) ? redirectURL : currentURL %>" />
                <portlet:param name="addressId" value="${phone.getAddressId()}" />
	</portlet:actionURL>

	<liferay-ui:icon iconCssClass="icon-minus-sign" url="<%= deleteURL %>" message="Delete address" onClick="if(!confirm('Are you sure?')) return false;"/>

	<portlet:renderURL var="editURL">
		<portlet:param name="mvcPath" value="/edit_address.jsp" />
		<portlet:param name="redirect" value="<%= currentURL %>" />
		<portlet:param name="customerId" value="<%= String.valueOf(customer.getCustomerId()) %>" />
                <portlet:param name="addressId" value="${address.getAddressId()}" />
	</portlet:renderURL>

	<liferay-ui:icon
		iconCssClass="icon-edit"
		message="edit"
		url="<%= editURL %>"
	/>

        <c:choose>
                <c:when test="${address.getType() == VISITING_ADDRESS}">
                        <liferay-ui:icon iconCssClass="icon-home" message="Visiting"/>
                </c:when>
                <c:when test="${address.getType() == POSTAL_ADDRESS}">
                        <liferay-ui:icon iconCssClass="icon-envelope" message="Postal" />
                </c:when>
        </c:choose>
        <c:out value="${address}" /><br/>
</c:forEach>

<portlet:renderURL var="editAddressURL">
        <portlet:param name="mvcPath" value="/edit_address.jsp" />
        <portlet:param name="customerId" value="<%= Long.toString(customer.getCustomerId()) %>" />
        <portlet:param name="redirect" value="<%= currentURL %>" />
</portlet:renderURL>

<liferay-ui:icon iconCssClass="icon-plus-sign" url="<%= editAddressURL %>" message="add-address" />

