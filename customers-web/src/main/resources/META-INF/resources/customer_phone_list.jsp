<%@ include file="/init.jsp" %>

<%
ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

Customer customer = null;

if (row != null) {
	customer = (Customer)row.getObject();
}
else {
	customer = (Customer)request.getAttribute("edit_customer.jsp-customer");
}
%>

<c:set value="<%= phoneLocalService.findByCustomerId(customer.getCustomerId()) %>" var="phones" />
<c:set var="MOBILE_PHONE" value="<%= EntityTypes.MOBILE_PHONE %>" />
<c:set var="WORK_PHONE" value="<%= EntityTypes.WORK_PHONE %>" />
<c:set var="HOME_PHONE" value="<%= EntityTypes.HOME_PHONE %>" />

<c:forEach items="${phones}" var="phone">
	<portlet:renderURL var="redirectURL">
		<portlet:param name="mvcPath" value="/view.jsp" />
	</portlet:renderURL>

	<portlet:actionURL var="deleteURL">
		<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.DELETE %>" />
		<portlet:param name="redirect" value="<%= (row == null) ? redirectURL : currentURL %>" />
                <portlet:param name="phoneId" value="${phone.getPhoneId()}" />
	</portlet:actionURL>

	<liferay-ui:icon iconCssClass="icon-minus-sign" url="<%= deleteURL %>" message="Delete phone" onClick="if(!confirm('Are you sure?')) return false;"/>

	<portlet:renderURL var="editURL">
		<portlet:param name="mvcPath" value="/edit_phone.jsp" />
		<portlet:param name="redirect" value="<%= currentURL %>" />
		<portlet:param name="customerId" value="<%= String.valueOf(customer.getCustomerId()) %>" />
                <portlet:param name="phoneId" value="${phone.getPhoneId()}" />
	</portlet:renderURL>

	<liferay-ui:icon
		iconCssClass="icon-edit"
		message="edit"
		url="<%= editURL %>"
	/>

        <c:choose>
                <c:when test="${phone.getType() == MOBILE_PHONE}">
                        <liferay-ui:icon iconCssClass="icon-phone" message="Mobile"/>
                </c:when>
                <c:when test="${phone.getType() == WORK_PHONE}">
                        <liferay-ui:icon iconCssClass="icon-briefcase" message="Work" />
                </c:when>
                <c:when test="${phone.getType() == HOME_PHONE}">
                        <liferay-ui:icon iconCssClass="icon-home" message="Home" />
                </c:when>
        </c:choose>
        <c:out value="${phone}" /><br/>
</c:forEach>

<portlet:renderURL var="editPhoneURL">
        <portlet:param name="mvcPath" value="/edit_phone.jsp" />
        <portlet:param name="customerId" value="<%= Long.toString(customer.getCustomerId()) %>" />
        <portlet:param name="redirect" value="<%= currentURL %>" />
</portlet:renderURL>

<liferay-ui:icon iconCssClass="icon-plus-sign" url="<%= editPhoneURL %>" message="add-phone" />

