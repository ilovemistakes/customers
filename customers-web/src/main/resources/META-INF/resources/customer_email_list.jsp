<%@ include file="/init.jsp" %>

<%
ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

Customer customer = null;

if (row != null) {
	customer = (Customer)row.getObject();
}
else {
	customer = (Customer)request.getAttribute("edit_customer.jsp-customer");
}
%>

<c:set value="<%= emailLocalService.findByCustomerId(customer.getCustomerId()) %>" var="emails" />
<c:set var="PERSONAL_EMAIL" value="<%= EntityTypes.PERSONAL_EMAIL %>" />
<c:set var="WORK_EMAIL" value="<%= EntityTypes.WORK_EMAIL %>" />

<c:forEach items="${emails}" var="email">
	<portlet:renderURL var="redirectURL">
		<portlet:param name="mvcPath" value="/view.jsp" />
	</portlet:renderURL>

	<portlet:actionURL var="deleteURL">
		<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.DELETE %>" />
		<portlet:param name="redirect" value="<%= (row == null) ? redirectURL : currentURL %>" />
                <portlet:param name="emailId" value="${email.getEmailId()}" />
	</portlet:actionURL>

	<liferay-ui:icon iconCssClass="icon-minus-sign" url="<%= deleteURL %>" message="Delete e-mail" onClick="if(!confirm('Are you sure?')) return false;"/>

	<portlet:renderURL var="editURL">
		<portlet:param name="mvcPath" value="/edit_email.jsp" />
		<portlet:param name="redirect" value="<%= currentURL %>" />
		<portlet:param name="customerId" value="<%= String.valueOf(customer.getCustomerId()) %>" />
                <portlet:param name="emailId" value="${email.getEmailId()}" />
	</portlet:renderURL>

	<liferay-ui:icon
		iconCssClass="icon-edit"
		message="edit"
		url="<%= editURL %>"
	/>

        <c:choose>
                <c:when test="${email.getType() == PERSONAL_EMAIL}">
                        <liferay-ui:icon iconCssClass="icon-user" message="Personal"/>
                </c:when>
                <c:when test="${email.getType() == WORK_EMAIL}">
                        <liferay-ui:icon iconCssClass="icon-briefcase" message="Work" />
                </c:when>
        </c:choose>
        <c:out value="${email}" /><br/>
</c:forEach>

<portlet:renderURL var="editEmailURL">
        <portlet:param name="mvcPath" value="/edit_email.jsp" />
        <portlet:param name="customerId" value="<%= Long.toString(customer.getCustomerId()) %>" />
        <portlet:param name="redirect" value="<%= currentURL %>" />
</portlet:renderURL>

<liferay-ui:icon iconCssClass="icon-plus-sign" url="<%= editEmailURL %>" message="add-email" />

