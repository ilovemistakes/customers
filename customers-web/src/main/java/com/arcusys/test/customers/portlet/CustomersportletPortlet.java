package com.arcusys.test.customers.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;

import java.io.IOException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.arcusys.test.customers.model.Customer;
import com.arcusys.test.customers.service.CustomerLocalService;
import com.arcusys.test.customers.model.Address;
import com.arcusys.test.customers.service.AddressLocalService;
import com.arcusys.test.customers.model.Phone;
import com.arcusys.test.customers.service.PhoneLocalService;
import com.arcusys.test.customers.model.Email;
import com.arcusys.test.customers.service.EmailLocalService;

@Component(
immediate = true,
property = {
        "com.liferay.portlet.display-category=category.sample",
        "com.liferay.portlet.instanceable=true",
        "javax.portlet.display-name=Customers",
        "javax.portlet.init-param.template-path=/",
        "javax.portlet.init-param.view-template=/view.jsp",
        "javax.portlet.resource-bundle=content.Language",
        "javax.portlet.security-role-ref=power-user,user"
},
service = Portlet.class
)
public class CustomersportletPortlet extends MVCPortlet {
        private CustomerLocalService _customerLocalService;
        private AddressLocalService _addressLocalService;
        private PhoneLocalService _phoneLocalService;
        private EmailLocalService _emailLocalService;
        private static Log _log = LogFactoryUtil.getLog(CustomersportletPortlet.class);

        protected void deleteCustomer(ActionRequest actionRequest) throws Exception {
                long customerId = ParamUtil.getLong(actionRequest, "customerId");

                getCustomerLocalService().deleteCustomer(customerId);
                // TODO: cascade delete relationships
        }

        protected void deleteEmail(ActionRequest actionRequest) throws Exception {
                long emailId = ParamUtil.getLong(actionRequest, "emailId");

                getEmailLocalService().deleteEmail(emailId);
        }

        protected void deletePhone(ActionRequest actionRequest) throws Exception {
                long phoneId = ParamUtil.getLong(actionRequest, "phoneId");

                getPhoneLocalService().deletePhone(phoneId);
        }

        protected void deleteAddress(ActionRequest actionRequest) throws Exception {
                long addressId = ParamUtil.getLong(actionRequest, "addressId");

                getAddressLocalService().deleteAddress(addressId);
        }

        protected void updateCustomer(ActionRequest actionRequest) throws Exception {
                long customerId = ParamUtil.getLong(actionRequest, "customerId");

                Customer customer;

                // fetch request parameters
                String name = ParamUtil.getString(actionRequest, "name");
                String notes = ParamUtil.getString(actionRequest, "notes");

                if (customerId <= 0) {
                        // create new customer object
                        customer = getCustomerLocalService().createCustomer(0);

                        customer.isNew();
                }
                else {
                        // fetch entity from DB
                        customer = getCustomerLocalService().fetchCustomer(customerId);

                        // copy-pasted from blade.servicebuilder.web sample
                        customer.setCustomerId(customerId);
                }

                // update fields
                customer.setName(name);
                customer.setNotes(notes);

                // persist changes
                if (customerId <= 0) {
                        getCustomerLocalService().addCustomerWithoutId(customer);
                }
                else {
                        getCustomerLocalService().updateCustomer(customer);
                }

        }

        protected void updateEmail(ActionRequest actionRequest) throws Exception {
                long emailId = ParamUtil.getLong(actionRequest, "emailId");
                long customerId = ParamUtil.getLong(actionRequest, "customerId");

                Email email;

                int type = ParamUtil.getInteger(actionRequest, "type");
                String value = ParamUtil.getString(actionRequest, "value");

                if (emailId <= 0) {
                        email = getEmailLocalService().createEmail(0);

                        email.isNew();
                        email.setCustomerId(customerId);
                }
                else {
                        email = getEmailLocalService().fetchEmail(emailId);

                        email.setEmailId(emailId);
                }

                email.setValue(value);
                email.setType(type);

                if (emailId <= 0) {
                        getEmailLocalService().addEmailWithoutId(email);
                }
                else {
                        getEmailLocalService().updateEmail(email);
                }
        }

        protected void updatePhone(ActionRequest actionRequest) throws Exception {
                long phoneId = ParamUtil.getLong(actionRequest, "phoneId");
                long customerId = ParamUtil.getLong(actionRequest, "customerId");

                Phone phone;

                int type = ParamUtil.getInteger(actionRequest, "type");
                String value = ParamUtil.getString(actionRequest, "value");

                if (phoneId <= 0) {
                        phone = getPhoneLocalService().createPhone(0);

                        phone.isNew();
                        phone.setCustomerId(customerId);
                }
                else {
                        phone = getPhoneLocalService().fetchPhone(phoneId);

                        phone.setPhoneId(phoneId);
                }

                phone.setValue(value);
                phone.setType(type);

                if (phoneId <= 0) {
                        getPhoneLocalService().addPhoneWithoutId(phone);
                }
                else {
                        getPhoneLocalService().updatePhone(phone);
                }
        }

        protected void updateAddress(ActionRequest actionRequest) throws Exception {
                long addressId = ParamUtil.getLong(actionRequest, "addressId");
                long customerId = ParamUtil.getLong(actionRequest, "customerId");

                Address address;

                int type = ParamUtil.getInteger(actionRequest, "type");
                String street = ParamUtil.getString(actionRequest, "street");
                String street2 = ParamUtil.getString(actionRequest, "street2");
                String postalCode = ParamUtil.getString(actionRequest, "postalCode");
                String town = ParamUtil.getString(actionRequest, "town");

                if (addressId <= 0) {
                        address = getAddressLocalService().createAddress(0);

                        address.isNew();
                        address.setCustomerId(customerId);
                }
                else {
                        address = getAddressLocalService().fetchAddress(addressId);

                        address.setAddressId(addressId);
                }

                address.setType(type);
                address.setStreet(street);
                address.setStreet2(street2);
                address.setPostalCode(postalCode);
                address.setTown(town);

                if (addressId <= 0) {
                        getAddressLocalService().addAddressWithoutId(address);
                }
                else {
                        getAddressLocalService().updateAddress(address);
                }
        }

        @Override
        public void processAction(ActionRequest actionRequest, ActionResponse actionResponse)
                throws IOException, PortletException {
                try {
                        String cmd = ParamUtil.getString(actionRequest, Constants.CMD);

                        // route request to a corresponding action method
                        if (cmd.equals(Constants.ADD) || cmd.equals(Constants.UPDATE)) {
                                if(actionRequest.getParameterMap().containsKey("emailId")) {
                                        updateEmail(actionRequest);
                                } else if(actionRequest.getParameterMap().containsKey("phoneId")) {
                                        updatePhone(actionRequest);
                                } else if(actionRequest.getParameterMap().containsKey("addressId")) {
                                        updateAddress(actionRequest);
                                } else if(actionRequest.getParameterMap().containsKey("customerId")) {
                                        updateCustomer(actionRequest);
                                }
                        }
                        else if (cmd.equals(Constants.DELETE)) {
                                if(actionRequest.getParameterMap().containsKey("emailId")) {
                                        deleteEmail(actionRequest);
                                } else if(actionRequest.getParameterMap().containsKey("phoneId")) {
                                        deletePhone(actionRequest);
                                } else if(actionRequest.getParameterMap().containsKey("addressId")) {
                                        deleteAddress(actionRequest);
                                } else if(actionRequest.getParameterMap().containsKey("customerId")) {
                                        deleteCustomer(actionRequest);
                                }
                        }

                        if (Validator.isNotNull(cmd)) {
                                if (SessionErrors.isEmpty(actionRequest)) {
                                        // report success
                                        SessionMessages.add(actionRequest, "operation-success");
                                }

                                // and redirect to the saved URL
                                String redirect = ParamUtil.getString(actionRequest, "redirect");

                                actionResponse.sendRedirect(redirect);
                        }
                }
                catch (Exception e) {
                        throw new PortletException(e);
                }

                super.processAction(actionRequest, actionResponse);
        }

        @Override
        public void render(RenderRequest renderRequest,
                        RenderResponse renderResponse) throws PortletException, IOException {

                //set service beans
                renderRequest.setAttribute("customerLocalService", getCustomerLocalService());
                renderRequest.setAttribute("addressLocalService", getAddressLocalService());
                renderRequest.setAttribute("phoneLocalService", getPhoneLocalService());
                renderRequest.setAttribute("emailLocalService", getEmailLocalService());

                super.render(renderRequest, renderResponse);

        }

        @Reference
        public void setCustomerLocalService(CustomerLocalService customerLocalService) {
                _customerLocalService = customerLocalService;
        }

        public CustomerLocalService getCustomerLocalService() {
                return _customerLocalService;
        }

        @Reference
        public void setAddressLocalService(AddressLocalService addressLocalService) {
                _addressLocalService = addressLocalService;
        }

        public AddressLocalService getAddressLocalService() {
                return _addressLocalService;
        }

        @Reference
        public void setPhoneLocalService(PhoneLocalService phoneLocalService) {
                _phoneLocalService = phoneLocalService;
        }

        public PhoneLocalService getPhoneLocalService() {
                return _phoneLocalService;
        }

        @Reference
        public void setEmailLocalService(EmailLocalService emailLocalService) {
                _emailLocalService = emailLocalService;
        }

        public EmailLocalService getEmailLocalService() {
                return _emailLocalService;
        }
}
