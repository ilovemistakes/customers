/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.arcusys.test.customers.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.arcusys.test.customers.model.Email;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Email in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Email
 * @generated
 */
@ProviderType
public class EmailCacheModel implements CacheModel<Email>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmailCacheModel)) {
			return false;
		}

		EmailCacheModel emailCacheModel = (EmailCacheModel)obj;

		if (emailId == emailCacheModel.emailId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, emailId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", emailId=");
		sb.append(emailId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", customerId=");
		sb.append(customerId);
		sb.append(", type=");
		sb.append(type);
		sb.append(", value=");
		sb.append(value);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Email toEntityModel() {
		EmailImpl emailImpl = new EmailImpl();

		if (uuid == null) {
			emailImpl.setUuid(StringPool.BLANK);
		}
		else {
			emailImpl.setUuid(uuid);
		}

		emailImpl.setEmailId(emailId);
		emailImpl.setCompanyId(companyId);
		emailImpl.setUserId(userId);

		if (userName == null) {
			emailImpl.setUserName(StringPool.BLANK);
		}
		else {
			emailImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			emailImpl.setCreateDate(null);
		}
		else {
			emailImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			emailImpl.setModifiedDate(null);
		}
		else {
			emailImpl.setModifiedDate(new Date(modifiedDate));
		}

		emailImpl.setCustomerId(customerId);
		emailImpl.setType(type);

		if (value == null) {
			emailImpl.setValue(StringPool.BLANK);
		}
		else {
			emailImpl.setValue(value);
		}

		emailImpl.resetOriginalValues();

		return emailImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		emailId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		customerId = objectInput.readLong();

		type = objectInput.readInt();
		value = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(emailId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(customerId);

		objectOutput.writeInt(type);

		if (value == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(value);
		}
	}

	public String uuid;
	public long emailId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long customerId;
	public int type;
	public String value;
}