/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.arcusys.test.customers.service.impl;

import aQute.bnd.annotation.ProviderType;

import com.arcusys.test.customers.model.Customer;
import com.arcusys.test.customers.service.base.CustomerLocalServiceBaseImpl;

/**
 * The implementation of the customer local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.arcusys.test.customers.service.CustomerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CustomerLocalServiceBaseImpl
 * @see com.arcusys.test.customers.service.CustomerLocalServiceUtil
 */
@ProviderType
public class CustomerLocalServiceImpl extends CustomerLocalServiceBaseImpl {
        /*
         * NOTE FOR DEVELOPERS:
         *
         * Never reference this class directly. Always use {@link com.arcusys.test.customers.service.CustomerLocalServiceUtil} to access the customer local service.
         */
        public Customer addCustomerWithoutId(Customer customer) {
                long resourcePrimKey = counterLocalService.increment();

                customer.setCustomerId(resourcePrimKey);

                return addCustomer(customer);
        }

}
