create index IX_75FC27E6 on Customer_Address (customerId);
create index IX_73BB394D on Customer_Address (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_895B2724 on Customer_Customer (name[$COLUMN_LENGTH:75$]);
create index IX_296269D9 on Customer_Customer (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_47A15D4E on Customer_Email (customerId);
create index IX_DD6CC0B5 on Customer_Email (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_A33DB7A0 on Customer_Phone (customerId);
create index IX_F69C9587 on Customer_Phone (uuid_[$COLUMN_LENGTH:75$], companyId);