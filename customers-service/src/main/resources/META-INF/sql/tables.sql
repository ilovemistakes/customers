create table Customer_Address (
	uuid_ VARCHAR(75) null,
	addressId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	customerId LONG,
	type_ INTEGER,
	street VARCHAR(75) null,
	street2 VARCHAR(75) null,
	postalCode VARCHAR(75) null,
	town VARCHAR(75) null
);

create table Customer_Customer (
	uuid_ VARCHAR(75) null,
	customerId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name VARCHAR(75) null,
	notes VARCHAR(75) null
);

create table Customer_Email (
	uuid_ VARCHAR(75) null,
	emailId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	customerId LONG,
	type_ INTEGER,
	value VARCHAR(75) null
);

create table Customer_Phone (
	uuid_ VARCHAR(75) null,
	phoneId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	customerId LONG,
	type_ INTEGER,
	value VARCHAR(75) null
);