/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.arcusys.test.customers.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Phone}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Phone
 * @generated
 */
@ProviderType
public class PhoneWrapper implements Phone, ModelWrapper<Phone> {
	public PhoneWrapper(Phone phone) {
		_phone = phone;
	}

	@Override
	public Class<?> getModelClass() {
		return Phone.class;
	}

	@Override
	public String getModelClassName() {
		return Phone.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("phoneId", getPhoneId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("customerId", getCustomerId());
		attributes.put("type", getType());
		attributes.put("value", getValue());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long phoneId = (Long)attributes.get("phoneId");

		if (phoneId != null) {
			setPhoneId(phoneId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long customerId = (Long)attributes.get("customerId");

		if (customerId != null) {
			setCustomerId(customerId);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}
	}

	@Override
	public Phone toEscapedModel() {
		return new PhoneWrapper(_phone.toEscapedModel());
	}

	@Override
	public Phone toUnescapedModel() {
		return new PhoneWrapper(_phone.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _phone.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _phone.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _phone.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _phone.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Phone> toCacheModel() {
		return _phone.toCacheModel();
	}

	@Override
	public int compareTo(Phone phone) {
		return _phone.compareTo(phone);
	}

	/**
	* Returns the type of this phone.
	*
	* @return the type of this phone
	*/
	@Override
	public int getType() {
		return _phone.getType();
	}

	@Override
	public int hashCode() {
		return _phone.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _phone.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new PhoneWrapper((Phone)_phone.clone());
	}

	/**
	* Returns the user name of this phone.
	*
	* @return the user name of this phone
	*/
	@Override
	public java.lang.String getUserName() {
		return _phone.getUserName();
	}

	/**
	* Returns the user uuid of this phone.
	*
	* @return the user uuid of this phone
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _phone.getUserUuid();
	}

	/**
	* Returns the uuid of this phone.
	*
	* @return the uuid of this phone
	*/
	@Override
	public java.lang.String getUuid() {
		return _phone.getUuid();
	}

	/**
	* Returns the value of this phone.
	*
	* @return the value of this phone
	*/
	@Override
	public java.lang.String getValue() {
		return _phone.getValue();
	}

	@Override
	public java.lang.String toString() {
		return _phone.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _phone.toXmlString();
	}

	/**
	* Returns the create date of this phone.
	*
	* @return the create date of this phone
	*/
	@Override
	public Date getCreateDate() {
		return _phone.getCreateDate();
	}

	/**
	* Returns the modified date of this phone.
	*
	* @return the modified date of this phone
	*/
	@Override
	public Date getModifiedDate() {
		return _phone.getModifiedDate();
	}

	/**
	* Returns the company ID of this phone.
	*
	* @return the company ID of this phone
	*/
	@Override
	public long getCompanyId() {
		return _phone.getCompanyId();
	}

	/**
	* Returns the customer ID of this phone.
	*
	* @return the customer ID of this phone
	*/
	@Override
	public long getCustomerId() {
		return _phone.getCustomerId();
	}

	/**
	* Returns the phone ID of this phone.
	*
	* @return the phone ID of this phone
	*/
	@Override
	public long getPhoneId() {
		return _phone.getPhoneId();
	}

	/**
	* Returns the primary key of this phone.
	*
	* @return the primary key of this phone
	*/
	@Override
	public long getPrimaryKey() {
		return _phone.getPrimaryKey();
	}

	/**
	* Returns the user ID of this phone.
	*
	* @return the user ID of this phone
	*/
	@Override
	public long getUserId() {
		return _phone.getUserId();
	}

	@Override
	public void persist() {
		_phone.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_phone.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this phone.
	*
	* @param companyId the company ID of this phone
	*/
	@Override
	public void setCompanyId(long companyId) {
		_phone.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this phone.
	*
	* @param createDate the create date of this phone
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_phone.setCreateDate(createDate);
	}

	/**
	* Sets the customer ID of this phone.
	*
	* @param customerId the customer ID of this phone
	*/
	@Override
	public void setCustomerId(long customerId) {
		_phone.setCustomerId(customerId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_phone.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_phone.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_phone.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the modified date of this phone.
	*
	* @param modifiedDate the modified date of this phone
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_phone.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_phone.setNew(n);
	}

	/**
	* Sets the phone ID of this phone.
	*
	* @param phoneId the phone ID of this phone
	*/
	@Override
	public void setPhoneId(long phoneId) {
		_phone.setPhoneId(phoneId);
	}

	/**
	* Sets the primary key of this phone.
	*
	* @param primaryKey the primary key of this phone
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_phone.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_phone.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the type of this phone.
	*
	* @param type the type of this phone
	*/
	@Override
	public void setType(int type) {
		_phone.setType(type);
	}

	/**
	* Sets the user ID of this phone.
	*
	* @param userId the user ID of this phone
	*/
	@Override
	public void setUserId(long userId) {
		_phone.setUserId(userId);
	}

	/**
	* Sets the user name of this phone.
	*
	* @param userName the user name of this phone
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_phone.setUserName(userName);
	}

	/**
	* Sets the user uuid of this phone.
	*
	* @param userUuid the user uuid of this phone
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_phone.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this phone.
	*
	* @param uuid the uuid of this phone
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_phone.setUuid(uuid);
	}

	/**
	* Sets the value of this phone.
	*
	* @param value the value of this phone
	*/
	@Override
	public void setValue(java.lang.String value) {
		_phone.setValue(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PhoneWrapper)) {
			return false;
		}

		PhoneWrapper phoneWrapper = (PhoneWrapper)obj;

		if (Objects.equals(_phone, phoneWrapper._phone)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _phone.getStagedModelType();
	}

	@Override
	public Phone getWrappedModel() {
		return _phone;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _phone.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _phone.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_phone.resetOriginalValues();
	}

	private final Phone _phone;
}