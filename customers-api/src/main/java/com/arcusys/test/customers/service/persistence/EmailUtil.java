/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.arcusys.test.customers.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.arcusys.test.customers.model.Email;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the email service. This utility wraps {@link com.arcusys.test.customers.service.persistence.impl.EmailPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailPersistence
 * @see com.arcusys.test.customers.service.persistence.impl.EmailPersistenceImpl
 * @generated
 */
@ProviderType
public class EmailUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Email email) {
		getPersistence().clearCache(email);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Email> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Email> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Email> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Email> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Email update(Email email) {
		return getPersistence().update(email);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Email update(Email email, ServiceContext serviceContext) {
		return getPersistence().update(email, serviceContext);
	}

	/**
	* Returns all the emails where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching emails
	*/
	public static List<Email> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the emails where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @return the range of matching emails
	*/
	public static List<Email> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the emails where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching emails
	*/
	public static List<Email> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Email> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the emails where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching emails
	*/
	public static List<Email> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Email> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first email in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email
	* @throws NoSuchEmailException if a matching email could not be found
	*/
	public static Email findByUuid_First(java.lang.String uuid,
		OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first email in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email, or <code>null</code> if a matching email could not be found
	*/
	public static Email fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Email> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last email in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email
	* @throws NoSuchEmailException if a matching email could not be found
	*/
	public static Email findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last email in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email, or <code>null</code> if a matching email could not be found
	*/
	public static Email fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Email> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the emails before and after the current email in the ordered set where uuid = &#63;.
	*
	* @param emailId the primary key of the current email
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next email
	* @throws NoSuchEmailException if a email with the primary key could not be found
	*/
	public static Email[] findByUuid_PrevAndNext(long emailId,
		java.lang.String uuid, OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence()
				   .findByUuid_PrevAndNext(emailId, uuid, orderByComparator);
	}

	/**
	* Removes all the emails where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of emails where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching emails
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the emails where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching emails
	*/
	public static List<Email> findByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the emails where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @return the range of matching emails
	*/
	public static List<Email> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching emails
	*/
	public static List<Email> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the emails where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching emails
	*/
	public static List<Email> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Email> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email
	* @throws NoSuchEmailException if a matching email could not be found
	*/
	public static Email findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first email in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email, or <code>null</code> if a matching email could not be found
	*/
	public static Email fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Email> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email
	* @throws NoSuchEmailException if a matching email could not be found
	*/
	public static Email findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last email in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email, or <code>null</code> if a matching email could not be found
	*/
	public static Email fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Email> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the emails before and after the current email in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param emailId the primary key of the current email
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next email
	* @throws NoSuchEmailException if a email with the primary key could not be found
	*/
	public static Email[] findByUuid_C_PrevAndNext(long emailId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(emailId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the emails where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of emails where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching emails
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the emails where customerId = &#63;.
	*
	* @param customerId the customer ID
	* @return the matching emails
	*/
	public static List<Email> findByCustomerId(long customerId) {
		return getPersistence().findByCustomerId(customerId);
	}

	/**
	* Returns a range of all the emails where customerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param customerId the customer ID
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @return the range of matching emails
	*/
	public static List<Email> findByCustomerId(long customerId, int start,
		int end) {
		return getPersistence().findByCustomerId(customerId, start, end);
	}

	/**
	* Returns an ordered range of all the emails where customerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param customerId the customer ID
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching emails
	*/
	public static List<Email> findByCustomerId(long customerId, int start,
		int end, OrderByComparator<Email> orderByComparator) {
		return getPersistence()
				   .findByCustomerId(customerId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the emails where customerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param customerId the customer ID
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching emails
	*/
	public static List<Email> findByCustomerId(long customerId, int start,
		int end, OrderByComparator<Email> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCustomerId(customerId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first email in the ordered set where customerId = &#63;.
	*
	* @param customerId the customer ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email
	* @throws NoSuchEmailException if a matching email could not be found
	*/
	public static Email findByCustomerId_First(long customerId,
		OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence()
				   .findByCustomerId_First(customerId, orderByComparator);
	}

	/**
	* Returns the first email in the ordered set where customerId = &#63;.
	*
	* @param customerId the customer ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email, or <code>null</code> if a matching email could not be found
	*/
	public static Email fetchByCustomerId_First(long customerId,
		OrderByComparator<Email> orderByComparator) {
		return getPersistence()
				   .fetchByCustomerId_First(customerId, orderByComparator);
	}

	/**
	* Returns the last email in the ordered set where customerId = &#63;.
	*
	* @param customerId the customer ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email
	* @throws NoSuchEmailException if a matching email could not be found
	*/
	public static Email findByCustomerId_Last(long customerId,
		OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence()
				   .findByCustomerId_Last(customerId, orderByComparator);
	}

	/**
	* Returns the last email in the ordered set where customerId = &#63;.
	*
	* @param customerId the customer ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email, or <code>null</code> if a matching email could not be found
	*/
	public static Email fetchByCustomerId_Last(long customerId,
		OrderByComparator<Email> orderByComparator) {
		return getPersistence()
				   .fetchByCustomerId_Last(customerId, orderByComparator);
	}

	/**
	* Returns the emails before and after the current email in the ordered set where customerId = &#63;.
	*
	* @param emailId the primary key of the current email
	* @param customerId the customer ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next email
	* @throws NoSuchEmailException if a email with the primary key could not be found
	*/
	public static Email[] findByCustomerId_PrevAndNext(long emailId,
		long customerId, OrderByComparator<Email> orderByComparator)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence()
				   .findByCustomerId_PrevAndNext(emailId, customerId,
			orderByComparator);
	}

	/**
	* Removes all the emails where customerId = &#63; from the database.
	*
	* @param customerId the customer ID
	*/
	public static void removeByCustomerId(long customerId) {
		getPersistence().removeByCustomerId(customerId);
	}

	/**
	* Returns the number of emails where customerId = &#63;.
	*
	* @param customerId the customer ID
	* @return the number of matching emails
	*/
	public static int countByCustomerId(long customerId) {
		return getPersistence().countByCustomerId(customerId);
	}

	/**
	* Caches the email in the entity cache if it is enabled.
	*
	* @param email the email
	*/
	public static void cacheResult(Email email) {
		getPersistence().cacheResult(email);
	}

	/**
	* Caches the emails in the entity cache if it is enabled.
	*
	* @param emails the emails
	*/
	public static void cacheResult(List<Email> emails) {
		getPersistence().cacheResult(emails);
	}

	/**
	* Creates a new email with the primary key. Does not add the email to the database.
	*
	* @param emailId the primary key for the new email
	* @return the new email
	*/
	public static Email create(long emailId) {
		return getPersistence().create(emailId);
	}

	/**
	* Removes the email with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param emailId the primary key of the email
	* @return the email that was removed
	* @throws NoSuchEmailException if a email with the primary key could not be found
	*/
	public static Email remove(long emailId)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence().remove(emailId);
	}

	public static Email updateImpl(Email email) {
		return getPersistence().updateImpl(email);
	}

	/**
	* Returns the email with the primary key or throws a {@link NoSuchEmailException} if it could not be found.
	*
	* @param emailId the primary key of the email
	* @return the email
	* @throws NoSuchEmailException if a email with the primary key could not be found
	*/
	public static Email findByPrimaryKey(long emailId)
		throws com.arcusys.test.customers.exception.NoSuchEmailException {
		return getPersistence().findByPrimaryKey(emailId);
	}

	/**
	* Returns the email with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param emailId the primary key of the email
	* @return the email, or <code>null</code> if a email with the primary key could not be found
	*/
	public static Email fetchByPrimaryKey(long emailId) {
		return getPersistence().fetchByPrimaryKey(emailId);
	}

	public static java.util.Map<java.io.Serializable, Email> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the emails.
	*
	* @return the emails
	*/
	public static List<Email> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @return the range of emails
	*/
	public static List<Email> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of emails
	*/
	public static List<Email> findAll(int start, int end,
		OrderByComparator<Email> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the emails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of emails
	* @param end the upper bound of the range of emails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of emails
	*/
	public static List<Email> findAll(int start, int end,
		OrderByComparator<Email> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the emails from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of emails.
	*
	* @return the number of emails
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static EmailPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<EmailPersistence, EmailPersistence> _serviceTracker =
		ServiceTrackerFactory.open(EmailPersistence.class);
}