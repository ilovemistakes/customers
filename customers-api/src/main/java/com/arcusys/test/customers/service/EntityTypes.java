package com.arcusys.test.customers.service;

// TODO: move to model layer somehow
public final class EntityTypes {
    public static final int VISITING_ADDRESS = 0;
    public static final int POSTAL_ADDRESS = 1;

    public static final int MOBILE_PHONE = 0;
    public static final int WORK_PHONE = 1;
    public static final int HOME_PHONE= 2;

    public static final int PERSONAL_EMAIL = 0;
    public static final int WORK_EMAIL = 1;
}
