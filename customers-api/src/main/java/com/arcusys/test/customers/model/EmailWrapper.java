/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.arcusys.test.customers.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Email}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Email
 * @generated
 */
@ProviderType
public class EmailWrapper implements Email, ModelWrapper<Email> {
	public EmailWrapper(Email email) {
		_email = email;
	}

	@Override
	public Class<?> getModelClass() {
		return Email.class;
	}

	@Override
	public String getModelClassName() {
		return Email.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("emailId", getEmailId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("customerId", getCustomerId());
		attributes.put("type", getType());
		attributes.put("value", getValue());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long emailId = (Long)attributes.get("emailId");

		if (emailId != null) {
			setEmailId(emailId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long customerId = (Long)attributes.get("customerId");

		if (customerId != null) {
			setCustomerId(customerId);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String value = (String)attributes.get("value");

		if (value != null) {
			setValue(value);
		}
	}

	@Override
	public Email toEscapedModel() {
		return new EmailWrapper(_email.toEscapedModel());
	}

	@Override
	public Email toUnescapedModel() {
		return new EmailWrapper(_email.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _email.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _email.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _email.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _email.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Email> toCacheModel() {
		return _email.toCacheModel();
	}

	@Override
	public int compareTo(Email email) {
		return _email.compareTo(email);
	}

	/**
	* Returns the type of this email.
	*
	* @return the type of this email
	*/
	@Override
	public int getType() {
		return _email.getType();
	}

	@Override
	public int hashCode() {
		return _email.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _email.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new EmailWrapper((Email)_email.clone());
	}

	/**
	* Returns the user name of this email.
	*
	* @return the user name of this email
	*/
	@Override
	public java.lang.String getUserName() {
		return _email.getUserName();
	}

	/**
	* Returns the user uuid of this email.
	*
	* @return the user uuid of this email
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _email.getUserUuid();
	}

	/**
	* Returns the uuid of this email.
	*
	* @return the uuid of this email
	*/
	@Override
	public java.lang.String getUuid() {
		return _email.getUuid();
	}

	/**
	* Returns the value of this email.
	*
	* @return the value of this email
	*/
	@Override
	public java.lang.String getValue() {
		return _email.getValue();
	}

	@Override
	public java.lang.String toString() {
		return _email.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _email.toXmlString();
	}

	/**
	* Returns the create date of this email.
	*
	* @return the create date of this email
	*/
	@Override
	public Date getCreateDate() {
		return _email.getCreateDate();
	}

	/**
	* Returns the modified date of this email.
	*
	* @return the modified date of this email
	*/
	@Override
	public Date getModifiedDate() {
		return _email.getModifiedDate();
	}

	/**
	* Returns the company ID of this email.
	*
	* @return the company ID of this email
	*/
	@Override
	public long getCompanyId() {
		return _email.getCompanyId();
	}

	/**
	* Returns the customer ID of this email.
	*
	* @return the customer ID of this email
	*/
	@Override
	public long getCustomerId() {
		return _email.getCustomerId();
	}

	/**
	* Returns the email ID of this email.
	*
	* @return the email ID of this email
	*/
	@Override
	public long getEmailId() {
		return _email.getEmailId();
	}

	/**
	* Returns the primary key of this email.
	*
	* @return the primary key of this email
	*/
	@Override
	public long getPrimaryKey() {
		return _email.getPrimaryKey();
	}

	/**
	* Returns the user ID of this email.
	*
	* @return the user ID of this email
	*/
	@Override
	public long getUserId() {
		return _email.getUserId();
	}

	@Override
	public void persist() {
		_email.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_email.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this email.
	*
	* @param companyId the company ID of this email
	*/
	@Override
	public void setCompanyId(long companyId) {
		_email.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this email.
	*
	* @param createDate the create date of this email
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_email.setCreateDate(createDate);
	}

	/**
	* Sets the customer ID of this email.
	*
	* @param customerId the customer ID of this email
	*/
	@Override
	public void setCustomerId(long customerId) {
		_email.setCustomerId(customerId);
	}

	/**
	* Sets the email ID of this email.
	*
	* @param emailId the email ID of this email
	*/
	@Override
	public void setEmailId(long emailId) {
		_email.setEmailId(emailId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_email.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_email.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_email.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the modified date of this email.
	*
	* @param modifiedDate the modified date of this email
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_email.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_email.setNew(n);
	}

	/**
	* Sets the primary key of this email.
	*
	* @param primaryKey the primary key of this email
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_email.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_email.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the type of this email.
	*
	* @param type the type of this email
	*/
	@Override
	public void setType(int type) {
		_email.setType(type);
	}

	/**
	* Sets the user ID of this email.
	*
	* @param userId the user ID of this email
	*/
	@Override
	public void setUserId(long userId) {
		_email.setUserId(userId);
	}

	/**
	* Sets the user name of this email.
	*
	* @param userName the user name of this email
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_email.setUserName(userName);
	}

	/**
	* Sets the user uuid of this email.
	*
	* @param userUuid the user uuid of this email
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_email.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this email.
	*
	* @param uuid the uuid of this email
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_email.setUuid(uuid);
	}

	/**
	* Sets the value of this email.
	*
	* @param value the value of this email
	*/
	@Override
	public void setValue(java.lang.String value) {
		_email.setValue(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmailWrapper)) {
			return false;
		}

		EmailWrapper emailWrapper = (EmailWrapper)obj;

		if (Objects.equals(_email, emailWrapper._email)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _email.getStagedModelType();
	}

	@Override
	public Email getWrappedModel() {
		return _email;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _email.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _email.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_email.resetOriginalValues();
	}

	private final Email _email;
}